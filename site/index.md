---
layout: default
---
# Introduction

This project is a tutorial to the x86 assembly language.

This project contains a [book](./tutorial.pdf).

Additionally a [system emulator](https://git.sw4j.net/osdevelopment-info/sys-emu) is created to emulate a system.
