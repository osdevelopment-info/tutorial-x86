NASM = /usr/bin/nasm
NASM_OPTS = -f bin

all: tex pdf

.PHONY: tex
tex: tutorial.nw
	$(MAKE) -C tex tex

pdf: tex
	cp ./*.tex tex/
	$(MAKE) -C tex pdf
	mkdir -p bin
	cp tex/tutorial.pdf bin/

mostlyclean:
	make -C tex mostlyclean
	rm -f *~
	rm -rf bin/

clean: mostlyclean
	make -C tex clean
