#!/bin/sh
cd $(dirname $0)
docker run -it -d -e "TERM=xterm-256color" -v `pwd`:`pwd` \
  -v $HOME/.bashrc:/root/.bashrc -w `pwd` \
  --name tutorial-x86-noweb -h tutorial-x86-noweb \
  registry.sw4j.net/sw4j-net/noweb/master:latest
docker run -it -d -e "TERM=xterm-256color" -v `pwd`:`pwd` \
  -v $HOME/.bashrc:/root/.bashrc -w `pwd` \
  --name tutorial-x86-nasm -h tutorial-x86-nasm \
  registry.sw4j.net/sw4j-net/nasm/master:latest
docker run -it -d -e "TERM=xterm-256color" -v `pwd`:`pwd` \
  -v $HOME/.bashrc:/root/.bashrc -w `pwd` \
  --name tutorial-x86-jekyll -h tutorial-x86-jekyll \
  registry.sw4j.net/sw4j-net/jekyll/master:latest
